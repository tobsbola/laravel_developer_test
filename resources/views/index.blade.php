
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>TEST</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">

</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-4">
            <form class="form form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="productName">Product Name</label>
                    <input type="text" class="form-control" id="productName" placeholder="Product Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Quantity in Stock</label>
                    <input type="text" class="form-control" id="quantity" placeholder="Quantity">
                </div>
                <div class="form-group">
                    <label for="unitPrice">Price Per Item (Unit Price)</label>
                    <input type="text" id="unitPrice">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-4">
            <table class="table table-striped">
                <tr>
                    <td class="active">S/N</td>
                    <td class="success">Product Name</td>
                    <td class="warning">Quantity</td>
                    <td class="danger">Unit Price</td>
                    <td class="info">actions</td>
                </tr>

{{--                {{ dd($data) }}--}}
                @if(!is_null($data) && isset($data))
                    @foreach($data as $da)
                        <tr>
                            <td class="active">{{ $da }}</td>
                            <td class="success"> </td>
                            <td class="warning"> </td>
                            <td class="danger"> </td>
                            <td class="info"> </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; ORIBOLAWALE_SAMSON_TOBI.</p>
    </footer>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
