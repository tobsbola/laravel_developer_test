<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //
    protected $fillable = ['product_name','unit_price','quantity'];


    public static function data()
    {
        $data =
            [
                'id'           => '1',
                'product_name' => 'MacBook',
                'unit_price'   => '30000',
                'quantity'     => '5',
            ];
            /*[
                'id'           => '2',
                'product_name' => 'Car',
                'unit_price'   => '40000',
                'quantity'     => '50',
            ]*/

        return $data;
    }
}
